FUNCTION_BLOCK fbGetHourAndMin
  VAR_INPUT
  END_VAR
  VAR_OUTPUT
            presentHour : USINT;
            presentMin  : USINT;
  END_VAR
  VAR_IN_OUT
  END_VAR
  VAR
     presentDatePLC : Date_and_time;
     dateTime    : TTecoDateTime;
  END_VAR
  VAR_TEMP
  END_VAR
         presentDatePLC := GetDateTime();
         dateTime := DT_TO_TecoDT(IEC_DT := presentDatePLC);
         presentHour := dateTime.hour;
         presentMin  := dateTime.min;

END_FUNCTION_BLOCK

