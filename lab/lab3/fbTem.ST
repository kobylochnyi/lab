FUNCTION_BLOCK fbTempLog
  VAR_INPUT
           temp : real;
  END_VAR
  VAR_OUTPUT
  END_VAR
  VAR_IN_OUT
  END_VAR
  VAR
     test_1 : fbTick;
     test_2 : fbTick;

     stop      : bool;
     firstLog  : bool;

  END_VAR
  VAR_TEMP
  END_VAR

         test_1(IN := true, PT := t#10s);
         test_2(In := true, PT := t#10ms);
         

         if not firstLog then
            toLog(level := DEBUG, source := 'fbTemp', event := 'Температура', comment := REAL_TO_STRING(temp));
            firstLog := not firstLog;
         end_if;

         if not stop then
               if test_1.Q then
                  toLog(level := DEBUG, source := 'fbTemp', event := 'Температура', comment := REAL_TO_STRING(temp));
               end_if;
               
               if test_2.Q then
                  stop := true;
               end_if;
         end_if;

END_FUNCTION_BLOCK
