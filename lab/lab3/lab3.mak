; Warning: This file is managed by Mosaic development environment.
; It is not recommended to change it manualy!

#program lab3 , V1.0
;**************************************
;<ActionName/>
;<Programmer/>
;<FirmName/>
;<Copyright/>
;**************************************
;<History>
;</History>
;**************************************
#useoption CPM = 9              ; CPM type: K
#useoption RemZone = 0          ; the remanent zone length
#useoption AlarmTime = 150      ; first alarm [milisec]
#useoption MaxCycleTime = 250   ; maximum cycle [milisec]
#useoption PLCstart = 1         ; cold start
#useoption AutoSummerTime = 0   ; internal PLC clock does not switch to daylight saving time
#useoption RestartOnError = 0   ; PLC will not be restarted after hard error

#uselib "LocalLib\StdLib_V21_20140514.mlb"
#uselib "LocalLib\SysLib_V35_20150416.mlb"
#uselib "LocalLib\ToStringLib_V13_20110203.mlb"
#uselib "LocalLib\ComLib_V21_20130528.mlb"
#uselib "LocalLib\FileLib_V20_20131202.mlb"
#uselib "LocalLib\TimeLib_V14_20130724.mlb"
#uselib "LocalLib\InternetLib_V31_20140422.mlb"
#uselib "LocalLib\CFoxLib_V13_20141021.mlb"
#uselib "LocalLib\RegoLib_V20_20121031.mlb"
#endlibs

;**************************************
#usefile "Sysgen\CIBMaker.st", 'auto'
#usefile "SysGen\HWConfig.ST", 'auto'
#usefile "Sysgen\CIBMaker.mos", 'auto'
#usefile "SysGen\lab3.hwc", 'auto'
#usefile "..\lab.hwn", 'auto'
#usefile "fbLogger.ST"
#usefile "fbTem.ST"
#usefile "fbButtonProcessing.ST"
#usefile "fbGetHourAndMin.ST"
#usefile "prgMain.ST"
#usefile "asd.ST"
#usefile "qwe.CFC"
#usefile "asd1.ST"
#usefile "prgPanel.ST"
#usefile "prgRCM2_Example.ST"
#usefile "lab3.mcf", 'auto'
