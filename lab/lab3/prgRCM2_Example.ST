VAR_GLOBAL RETAIN
           WeekProg : _TimeProg2_Week_ := (
                    Mon := ( T_ON1 := T#6h, T_OFF1 := T#7h,
                        T_ON2 := T#16h, T_OFF2 := T#22h),
                    Tue := ( T_ON1 := T#6h, T_OFF1 := T#7h,
                        T_ON2 := T#16h, T_OFF2 := T#22h),
                    Wed := ( T_ON1 := T#6h, T_OFF1 := T#7h,
                        T_ON2 := T#16h, T_OFF2 := T#22h),
                    Thu := ( T_ON1 := T#6h, T_OFF1 := T#7h,
                        T_ON2 := T#16h, T_OFF2 := T#22h),
                    Fri := ( T_ON1 := T#6h, T_OFF1 := T#7h,
                        T_ON2 := T#16h, T_OFF2 := T#22h),
                    Sat := ( T_ON1 := T#8h, T_OFF1 := T#22h,
                        T_ON2 := T#0s, T_OFF2 := T#0s),
                    Sun := ( T_ON1 := T#8h, T_OFF1 := T#22h,
                        T_ON2 := T#0s, T_OFF2 := T#0s));
                        
           TempReqLo : REAL := 21.0;
           TempReqHi : REAL := 23.0;
           TimeAlarm : TIME := T#7h;

END_VAR

VAR_GLOBAL
          RCM_Screens : ARRAY [1..5] OF TRCM2_1_Screen;
          
          ClockScr AT RCM_Screens[1] : TRCM2_1_Screen :=
                   (IsTime := true, BlinkingDots := true,
                   Symbols := ( Clock := true));
                   
          RoomTempScr AT RCM_Screens[2] : TRCM2_1_Screen :=
                      (ShowTenths := true,
                      Symbols := ( Thermometer := true, Celsius := true));
                      
          ReqTempHiScr AT RCM_Screens[3] : TRCM2_1_Screen :=
                       (RealInc := 0.5, RealMin := 15.0, RealMax := 30.0,
                       ShowTenths := true, Editable := true,
                       Symbols := ( Thermometer := true, Sun := true,
                       Spanner := true, Celsius := true));
                       
          ReqTempLoScr AT RCM_Screens[4] : TRCM2_1_Screen :=
                       (RealInc := 0.5, RealMin := 15.0, RealMax := 30.0,
                       ShowTenths := true, Editable := true,
                       Symbols := ( Thermometer := true, Moon := true,
          Spanner := true, Celsius := true));
          
          AlarmSetScr AT RCM_Screens[5] : TRCM2_1_Screen :=
                      (TimeMin := T#0s, TimeMax := T#23h59m,
                      IsTime := true, Editable := true,
                      Symbols := ( Clock := true, Spanner := true,
                      Bell := true));
END_VAR
PROGRAM prgRCM2_Example
  VAR_INPUT
  END_VAR
  VAR_OUTPUT
  END_VAR
  VAR
     Prog2 : TProg2;
     RCM2_1 : fbRCM2_1;
     RCM2_1_BackToFirst : TON := (PT := T#1m);
     LastScreen : UINT;
  END_VAR
  VAR_TEMP
  END_VAR
         Prog2(TPg := WeekProg);
         
         ClockScr.TimeValue := GetTime();
         ClockScr.Symbols.Moon := NOT Prog2.Out;
         ClockScr.Symbols.Sun := Prog2.Out;


         RoomTempScr.RealValue := RCM2_1_IN.eTHERM;
         RoomTempScr.Symbols.Moon := NOT Prog2.Out;
         RoomTempScr.Symbols.Sun := Prog2.Out;
         
         RCM2_1_SyncVarReal(Val := TempReqHi, Screen := ReqTempHiScr);
         RCM2_1_SyncVarReal(Val := TempReqLo, Screen := ReqTempLoScr);
         RCM2_1_SyncVarTime(Val := TimeAlarm, Screen := AlarmSetScr);
         
         RCM2_1(Press := RCM2_1_IN.FLG.PRESS,
         Counter := RCM2_1_IN.Counter,
         MaxScr := 5,
         Screens := RCM_Screens[1],
         RCM_OUT := void(RCM2_1_OUT));
         
         IF NOT RCM_Screens[RCM2_1.ActScr].Editable THEN
            RCM2_1_OUT.ICO.ONE := System_S.COUNTER_DAYS_OF_WEEK = 1;
            RCM2_1_OUT.ICO.TWO := System_S.COUNTER_DAYS_OF_WEEK = 2;
            RCM2_1_OUT.ICO.THREE := System_S.COUNTER_DAYS_OF_WEEK = 3;
            RCM2_1_OUT.ICO.FOUR := System_S.COUNTER_DAYS_OF_WEEK = 4;
            RCM2_1_OUT.ICO.FIVE := System_S.COUNTER_DAYS_OF_WEEK = 5;
            RCM2_1_OUT.ICO.SIX := System_S.COUNTER_DAYS_OF_WEEK = 6;
            RCM2_1_OUT.ICO.SEVEN := System_S.COUNTER_DAYS_OF_WEEK = 7;
         END_IF;
         
         RCM2_1_BackToFirst(IN := NOT RCM2_1.Edit AND
         RCM2_1.ActScr = LastScreen AND
         RCM2_1.ActScr <> 1);

         LastScreen := RCM2_1.ActScr;
         
         IF RCM2_1_BackToFirst.Q THEN
            RCM2_1.SetScr := 1;
            RCM2_1.ScrNo := 1;
         END_IF;

END_PROGRAM

