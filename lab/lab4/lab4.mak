; Warning: This file is managed by Mosaic development environment.
; It is not recommended to change it manualy!

#program lab4 , V1.0
;**************************************
;<ActionName/>
;<Programmer/>
;<FirmName/>
;<Copyright/>
;**************************************
;<History>
;</History>
;**************************************
#useoption CPM = 9              ; CPM type: K
#useoption RemZone = 128        ; the remanent zone length
#useoption AlarmTime = 150      ; first alarm [milisec]
#useoption MaxCycleTime = 250   ; maximum cycle [milisec]
#useoption PLCstart = 1         ; cold start
#useoption AutoSummerTime = 0   ; internal PLC clock does not switch to daylight saving time
#useoption RestartOnError = 0   ; PLC will not be restarted after hard error

#uselib "LocalLib\StdLib_V21_20140514.mlb"
#uselib "LocalLib\SysLib_V35_20150416.mlb"
#uselib "LocalLib\CFoxLib_V13_20141021.mlb"
#uselib "LocalLib\BuildingLib_V13_20140707.mlb"
#uselib "LocalLib\RegoLib_V20_20121031.mlb"
#endlibs

;**************************************
#usefile "Sysgen\CIBMaker.st", 'auto'
#usefile "SysGen\HWConfig.ST", 'auto'
#usefile "Sysgen\CIBMaker.mos", 'auto'
#usefile "SysGen\lab4.hwc", 'auto'
#usefile "..\lab.hwn", 'auto'
#usefile "LAB4.ST"
#usefile "fb_yatun_termostat.ST"
#usefile "fbShtuka.ST"
#usefile "fbRCM.ST"
#usefile "prgMain.ST"
#usefile "prgMain2.ST"
#usefile "asd.CFC"
#usefile "lab4.mcf", 'auto'
